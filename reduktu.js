// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

var r = (() => {

const CONF_FILENAME = 'conf.json';
const SCRIPT_FILENAME = 'script.js';
const DATA_FILENAME = 'data.json';
const TEMPLATE_FILENAME = 'template.html';
const STYLE_FILENAME = 'style.css';
const PARTIAL_DEFAULT_PARAM = 'title';
const PARTIAL_DEFAULT_TARGET = 'body';

const logTypes = {
    none: 0,
    save: 1,
    err:  2,
    warn: 3,
    all:  4,
    dom:  5
};
let logsLevel = logTypes.none;
let savedLogs = [];

let inited = false;

const dirs = {
    img:        '/assets/img/',
    css:        '/assets/css/',
    js:         '/assets/js/',
    partials:   '/partials/', 
};

const
    partials = [],
    scripts = [],
    styles = [],
    cssClasses = [],
    loadedPartials = [],
    functions = {},
    data = {},
    params = {},
    shared = {};
    
function logs(level) {
    if (level === undefined) {
        return logsLevel > logTypes.none ? savedLogs : null;
    }
    
    if (typeof level === 'number') {
        logsLevel = level > logTypes.none ? level : logTypes.none;
    }
    
    return logsLevel;
}

function log(msg, logType = logTypes.save) {
    if (logsLevel > logTypes.none) {
        savedLogs.push(msg);
    }
    
    if (logsLevel > logTypes.warn || logsLevel >= logTypes.warn && logType === logTypes.warn || logsLevel >= logTypes.err && logType === logTypes.err) {
        console.log(`%c ${Date.now()} ${msg}`, `color: ${logType === logTypes.err ? 'red; font-weight:bolder' : 'gray'};`);
    }
}

function showErr(msg, target) {
    const errMsg = 'ERROR: ' + msg;
    
    log(errMsg, logTypes.err);
    
    if (logsLevel >= logTypes.dom && target) {
        const domElem = document.querySelector(target);
        
        domElem.insertAdjacentHTML('afterbegin', `
            <div style="display: inline-block; width: 100%; height: 100%; border: 1px dotted red;">
                ${errMsg}
            </div>
        `);
    }
}

function resolvePath(dir, item) {
    if (item.includes('http')) {
        return item;
    }
    
    let basePath = dir.endsWith('/') ? dir : dir + '/';
    
    return `${basePath}${item}`
}

function registerPartialMainFunction(content, partialName){
    if (functions[partialName] || content.indexOf('partialExec') === -1) {
        return content;
    }
    
    const registeredFuncName = partialName + 'PartialRegisteredMainExecFunction';
    let res = content.replace('partialExec', registeredFuncName);
    
    functions[partialName] = registeredFuncName;
    
    return res;
}

async function fetchContent(url, responseType = 'text') {
    let content = null;
    
    try {
        const response = await fetch(url);
        
        if (!response.ok) {
            return null;
        }
        
        content = await response[responseType]();
        log('Loaded: content ' + url);
    }
    catch {
        showErr('Failed loading ' + url);
        return null;
    }
    
    return content;
}

function toFragment(html) {
    var tmp = document.createElement('template');
    
    tmp.innerHTML = html;
    
    return tmp.content;
}

function partialAlreadyLoaded(partialName) {
    if (loadedPartials.includes(partialName)) {
        return true;
    }
    
    return false;
}

function partialID(partialName) {
    let res = partialName;
    
    if (partialAlreadyLoaded(partialName)) {
        const l = loadedPartials.filter(n => n.startsWith(partialName));
        
        res += l.length + 1;
    }
    
    return res;
}

async function partialExec(name, fragment = null) {
    if (!functions[name]) {
        return fragment;
    }
    
    const res = await window[functions[name]](fragment, params[name], data[name], shared);
    
    if (!res) {
        return fragment;
    }
    
    if (res instanceof Node) {
        return res;
    }
    
    const { partial = null, ...sharedValues } = res;
    Object.assign(shared, sharedValues);
    
    return partial;
}

async function loadScript(url, partialName = null) {
    try {
        let content = await fetchContent(url);
        
        if (!content) {
            return false;
        }
        
        if (partialName) {
            content = registerPartialMainFunction(content, partialName);
        }
        
        let script = document.createElement('script');

        script.type = 'text/javascript';
        script.async = true; 
        script.innerHTML = content;
        log('Loaded: script ' + url);
        
        await document.head.appendChild(script);
    }
    catch {
        return false;
    }
    
    return true;
}

async function loadStyle(url, media = 'screen,print') {
    try {
        let link = document.createElement('link');
        
        link.type = "text/css";
        link.rel = "stylesheet";
        link.media = media;
        link.onload = () => log('Loaded: style ' + url);
        link.href = url;
        
        await document.head.appendChild(link);
    }
    catch {
        return false;
    }
    
    return true;
}

async function partialParams(item) {
    if (!item.params) {
        return false;
    }
    
    if (!params[item.name]) {
        params[item.name] = {};
    }
    
    Object.assign(params[item.name], item.params);
    
    if (item.defaultParam && item.defaultParamValue !== null) {
        params[item.name][item.defaultParam] = item.defaultParamValue;
    }
    
    log('Loaded: partial params ' + item.name);
    
    return true;
}

async function partialData(item) {
    if (partialAlreadyLoaded(item.name)) {
        return false;
    }
    
    if (!item.parts.data) {
        return false;
    }

    const partialData = await fetchContent(typeof item.parts.data === 'boolean' ? `${item.baseDir}/${DATA_FILENAME}` : item.parts.data, 'json');
    
    if (partialData) {
        data[item.name] = partialData;
    }
    else {
        data[item.name] = null;
    }
    
    log('Loaded: partial data ' + item.name);
    
    return true;
}

async function partialTemplate(item) {
    if (!item.parts.template) {
        await partialExec(item.name);
        return false;
    }

    const html = await fetchContent(`${item.baseDir}/${TEMPLATE_FILENAME}`);
    
    if (!html) {
        return false;
    }

    let fragment = toFragment(html);
    let target = document.querySelector(item.target);
    
    if (!target) {
        showErr('template target ' + item.name + ': ' + item.target);
        return false;
    }
    
    let pos = item.begin ? target.firstChild : null;

    if (item.parts.data && !data[item.name]) {
        showErr('template data ' + item.name, item.target);
        return false;
    }
    
    if (item.params && !params[item.name]) {
        showErr('template params ' + item.name, item.target);
        return false;
    }
                
    fragment = await partialExec(item.name, fragment);
    
    if (!fragment) {
        showErr('template ' + item.name, item.target);
        return false;
    }
    
    if (fragment.childElementCount === 1 && fragment.firstElementChild.id === '') {
        fragment.firstElementChild.setAttribute('id', item.id);
    }
    
    target.insertBefore(fragment, pos);
    
    log('Loaded: partial template ' + item.name);
    
    return true;
}

async function partialScripts(item) {
    if (partialAlreadyLoaded(item.name)) {
        return false;
    }
    
    log('Loading partial scripts ' + item.name);
    
    if (item.scripts) {
        for await (const s  of item.scripts) {
            await loadScript(s);
        }
    }
    
    if (item.parts.script) {
        await loadScript(`${item.baseDir}/${SCRIPT_FILENAME}`, item.name);
    }
    
    return true;
}

async function partialStyles(item) {
    if (partialAlreadyLoaded(item.name)) {
        return false;
    }
    
    log('Loading partial styles ' + item.name);
    
    if (item.styles) {
        for await (const s  of item.styles) {
            await loadStyle(s);
        }
    }
    
    if (item.parts.style) {
        await loadStyle(`${item.baseDir}/${STYLE_FILENAME}`);
    }
    
    return true;
}

async function loadPartial(item) {
    let partialName = null;
    let defaultParamValue = null;
    
    // r.add('partialname');
    if (typeof item === 'string') {
        partialName = item;
    }
    // r.add({ 'partialname': 'value' });
    else if (typeof item === 'object' && Object.keys(item).length === 1) {
        partialName = Object.keys(item)[0];
        defaultParamValue = item[partialName];
    }
    // r.add({ name: 'partialname'[, ...] });
    else if (typeof item === 'object' && item.name) {
        partialName = item.name;
    }
    else {
        showErr(`add(${item})`);
        return false;
    }
    
    const baseDir = resolvePath(dirs.partials, partialName);
    const partialConf = await fetchContent(`${baseDir}/${CONF_FILENAME}`, 'json');
    
    if (!partialConf || !partialConf.parts) {
        return false;
    }
    
    partialConf.id = partialID(partialName);
    partialConf.name = partialName;
    partialConf.baseDir = baseDir;
    partialConf.defaultParam = item.defaultParam || partialConf.defaultParam || null;
    partialConf.defaultParamValue = defaultParamValue;
    partialConf.target = item.target || partialConf.target || PARTIAL_DEFAULT_TARGET;
    partialConf.begin = item.begin || partialConf.begin || null;
    
    if (partialConf.params) {
        Object.assign(partialConf.params, item.params || {});
    }
    else {
        partialConf.params = item.params || {};
    }
    
    log('Loading partial ' + partialName);
    
    const pParams = await partialParams(partialConf);
    const pData = await partialData(partialConf);
    const pScripts = await partialScripts(partialConf);
    const pTemplate = await partialTemplate(partialConf);
    const pStyles = await partialStyles(partialConf);
    
    loadedPartials.push(partialName);
    log('Loaded partials: ' + loadedPartials.toString());
    
    return {
        name: partialName,
        params: pParams,
        data: pData,
        template: pTemplate,
        scripts: pScripts,
        styles: pStyles
    };
}

async function loadConf() {
    const conf = await fetchContent('/' + CONF_FILENAME, 'json');
    
    if (!conf) {
        return false;
    }
    
    if (conf.scripts) scripts.push(...conf.scripts);
    if (conf.styles) styles.push(...conf.styles);
    if (conf.cssClasses) cssClasses.push(...conf.cssClasses);
    if (conf.partials) partials.push(...conf.partials);
    if (conf.dirs) await Object.assign(dirs, conf.dirs);
    
    return true;
}

async function loadScripts() {
    if (scripts === undefined || scripts.length === 0) {
        return false;
    }

    for await (const item of scripts) {
        let url = resolvePath(dirs.js, item);
        
        await loadScript(item);
    }
    
    return true;
}

async function loadStyles() {
    if (styles === undefined || styles.length === 0) {
        return false;
    }
     
    for await (const item of styles) {
        let url = resolvePath(dirs.css, item);
        
        await loadStyle(url);
    }
    
    return true;
}

async function addCssClasses() {
    if (cssClasses === undefined || cssClasses.length === 0) {
        return false;
    }
    
    for await (const item of cssClasses) {
        let el = document.querySelector(Object.values(item)[0]);
        let css = Object.keys(item)[0].split(' ');
        
        el.classList.add(...css);
    }
    
    return true;
}

async function loadPartials() {
    if (partials === undefined || partials.entries().length === 0) {
        return false;
    }

    for await (const [n, item] of partials.entries()) {
        await loadPartial(item);
    }
    
    return true;
}

async function loadImages(fragment) {
    let target = fragment ? fragment : document;
    let images = target.querySelectorAll('img[data-src]');
    
    log(`Loading ${images.length} images`);
    
    for await (const img of images) {
        let src = resolvePath(dirs.img, img.dataset.src);
        
        img.src = src;
        delete img.dataset.src;
    }
    
    log(`Loaded: ${images.length} images`);
    
    return true;
}

async function add(...items) {
    if (!inited) {
        partials.push(...items);
    }
    else {
        for await (const item of items) {
            await loadPartial(item);
        }
    }
}

(async () => {
    if (inited) return;
    
    if (window.location.protocol === "http:") {
        logs(logTypes.dom);
    }
    
    await loadConf();
    await loadScripts();
    await loadStyles();
    await loadPartials();
    await addCssClasses();
    await loadImages();
    
    inited = true;
})();

return {
    add: add,
    fetch: fetchContent,
    logs: {
        set:    (level) => { return logs(level); },
        list:   logs()
    }
}

})();

// @license-end
